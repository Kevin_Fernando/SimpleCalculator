let firstNumbers = [];
let secondNumbers = [];

function init()
{
    const content = document.querySelectorAll('.content');

    processingNodes(content);
}

function processingNodes(node)
{   
    operated = false;

    for(let i = 0; i < node.length; i++)
    {   
        node[i].addEventListener("click", (event)=>{
            if(node[i].textContent === '-' || node[i].textContent === '+')
            {
                operated = true;
            }

            else if(node[i].textContent === '=')
            {   
                calculate(firstNumbers, secondNumbers);
            }

            else
            {
                putNumbers(node[i].textContent, operated);
            }
        })  
    }
}

function putNumbers(userNumber, operated=false)
{   
    userNumber = parseInt(userNumber);

    if(!operated)
    {
        firstNumbers.push(userNumber);
    }

    else
    {
        secondNumbers.push(userNumber);
    }
}

function calculate(arraySet1, arraySet2)
{
    arraySet1.forEach(element => {
        console.log(element);
    });
}


window.onload = init();
